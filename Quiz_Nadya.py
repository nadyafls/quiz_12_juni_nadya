#!/usr/bin/env python
# coding: utf-8

# In[5]:


API_KEY='P4opscCYayfeU7OQFnkqh7WYQ'
API_SECRET_KEY='g2pi4dzjfOYYSoxWyZWHmtlSjUicAizfaKiVM20zvAOQJ8eNo1'
ACCESS_TOKEN='1167359584446513152-HTnVMVfwS0AaJyMLGvmA4genAT2ewO'
ACCESS_TOKEN_SECRET='fnC3UhDvrDox9XGT6pPivRQYk42Wpt8XK7V3VUrzllAFQ'


# In[6]:


import tweepy
import json
import datetime

auth = tweepy.OAuthHandler(API_KEY, API_SECRET_KEY)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)


# In[7]:


# Scarping Data by Range Date
#scrapping function
search_terms = ['Covid19','Corona']
date_since = datetime.datetime(2020, 2, 11, 0, 0, 0)
date_until = datetime.datetime(2020, 6, 10, 0, 0, 0)
def stream_tweets(search_term):
    Data1 = []
    counter = 0
    for tweet in tweepy.Cursor(api.search, q='\"{}" -filter:retweets'.format(search_term),
                               count=100,lang='en',since= date_since,until=date_until,
                               tweet_mode='extended').items():
        tweet_details = {}
        tweet_details['name'] = tweet.user.screen_name
        tweet_details['tweet'] = tweet.full_text
        tweet_details['retweets'] = tweet.retweet_count
        tweet_details['location'] = tweet.user.location
        tweet_details['created'] = tweet.created_at.strftime("%d-%b-%Y")
        tweet_details['followers'] = tweet.user.followers_count
        tweet_details['is_user_verified'] = tweet.user.verified
        Data1.append(tweet_details)
        
        counter += 1

    with open('Data1/{}.json'.format(search_term), 'w') as f:
        json.dump(Data1, f)
    print('done!')


# In[8]:


#Running Script
if __name__ == "__main__":
    print ('Starting to stream...')
    for search_term in search_terms:
        stream_tweets(search_term)
    print ('Finished!')


# In[9]:


# Change Data Format (JSON -> CSC)
import pandas as pd
df = pd.read_json (r'C:\Users\ASUS\Desktop\Lab IP\Data1\Corona.json')
df.to_csv (r'C:\Users\ASUS\Desktop\Lab IP\Data1\Corona.csv', index = None)


# In[10]:


df1 = pd.read_json (r'C:\Users\ASUS\Desktop\Lab IP\Data1\Covid19.json')
df1.to_csv (r'C:\Users\ASUS\Desktop\Lab IP\Data1\Covid19.csv', index = None)


# In[156]:


# Cleaning data
import pandas as pd
import numpy as np
import re

#cleaning data
def clean_tweet(tweet):
    return ' '.join(re.sub('(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)', '', tweet).split())


# In[157]:


# Creat Data Frame
def create_corona_df():
    corona_df = pd.read_json ('Data1/Corona.json', orient = 'records')
    corona_df['clean_tweet'] = corona_df['tweet'].apply(lambda x: clean_tweet(x))
    corona_df['clean_location'] = corona_df['location'].apply(lambda x: clean_tweet(x))
    corona_df['search_name'] = 'Corona'
    corona_df.drop_duplicates(subset=['name'], keep ='first',inplace = True)
    return corona_df

def create_covid19_df():
    covid19_df = pd.read_json ('Data1/Covid19.json', orient = 'records')
    covid19_df['clean_tweet'] = covid19_df['tweet'].apply(lambda x: clean_tweet(x))
    covid19_df['clean_location'] = covid19_df['location'].apply(lambda x: clean_tweet(x))
    covid19_df['search_name'] = 'Covid19'
    covid19_df.drop_duplicates(subset=['name'], keep ='first',inplace = True)
    return covid19_df

creat_corona_df()
creat_covid19_df()


# In[158]:


# Join Data Frame
def join_dfs():
    corona_df = creat_corona_df()
    covid19_df = creat_covid19_df()
    frames = [corona_df,covid19_df]
    searchname_df = pd.concat(frames, ignore_index = True)
    return searchname_df

join_dfs()


# In[159]:


#Analyze Data
def analyze():
    searchname_df = join_dfs()
    
    #Amount of corona tweet by location
    corona_location = create_corona_df()
    corona_by_location = corona_location.groupby('clean_location')['name'].count().reset_index()
    corona_by_location.columns = ['location_name','amount_of_corona_tweets']
    
    #Amount of covid19 tweet by location
    covid19_location = create_covid19_df()
    covid19_by_location = covid19_location.groupby('clean_location')['name'].count().reset_index()
    covid19_by_location.columns =['location_name','amount_of_covid19_tweets']
    
    #Average of follower by keywords
    followers_of_user    = searchname_df.groupby('search_name')['followers'].mean().reset_index()
    followers_of_user.columns = ['keyword_name','average_of_followers_of_user']
    
    #Average of retweet by keywords
    retweet_user = searchname_df.groupby('search_name')['retweets'].mean().reset_index()
    retweet_user.columns = ['keyword_name','average_of_retweets_of_user']
    
    return (corona_by_location.sort_values(by='amount_of_corona_tweets',ascending = False).head(5),
            covid19_by_location.sort_values(by='amount_of_covid19_tweets',ascending = False).head(5),
            followers_of_user,retweet_user)

analyze()


# In[160]:


#plotting Data to Chart

from matplotlib import pyplot as plt

def plot_graphs():
    analysis_details = analyze()
    corona_by_location,covid19_by_location,followers_of_user,retweet_user = analysis_details
    
    # Bar Chart for Amount of corona tweet by location
    fig1,ax1, = plt.subplots(figsize=(10,5))
    ax1.bar(corona_by_location['location_name'],corona_by_location['amount_of_corona_tweets'],
           label = 'Amount by location_name')
    ax1.set_xlabel('location')
    ax1.set_ylabel('Amount')
    ax1.set_title('Amount of corona tweet by location')
    
    # Bar Chart for Amount of covid19 tweets by location
    fig2,ax2, = plt.subplots(figsize=(10,5))
    ax2.bar(covid19_by_location['location_name'],covid19_by_location['amount_of_covid19_tweets'],
           label = 'Amount by location_name')
    ax2.set_xlabel('location')
    ax2.set_ylabel('Amount')
    ax2.set_title('Amount of covid19 tweet by location')
    
    # Bar Chart for Average of followers by keywords
    fig3, ax3, = plt.subplots(figsize=(10,5))
    ax3.bar(followers_of_user['keyword_name'], followers_of_user['average_of_followers_of_user'],
            label = 'Average by keyword_name')
    ax3.set_xlabel('Keyword')
    ax3.set_ylabel('Average')
    ax3.set_title('Average of Followers by Keyword')
    
    # Bar Chart for Average of retweets by keywords
    fig4, ax4, = plt.subplots(figsize=(10,5))
    ax4.bar(retweet_user['keyword_name'], retweet_user['average_of_retweets_of_user'],
            label = 'Average by keyword_name')
    ax4.set_xlabel('Keyword')
    ax4.set_ylabel('Average')
    ax4.set_title('Average of retweet by Keyword')

    list_of_figures = [plt.figure(i) for i in plt.get_fignums()]
    return list_of_figures


# In[161]:


# running Programs 
import re

plot_graphs()


# In[151]:


def analyze ():
    seearchname_df=join_dfs()
    
    corona_created = create_corona_df()
    corona_by_created=corona_created.groupby('created')['tweet'].count().reset_index()
    corona_by_created.columns = ['created','numbers_of_tweet']
    
    covid19_created = create_covid19_df()
    covid19_by_created=covid19_created.groupby('created')['tweet'].count().reset_index()
    covid19_by_created.columns = ['created','numbers_of_tweet']
    
    return (corona_by_created,covid19_by_created)

analyze()


# In[152]:


from matplotlib import pyplot as plt

def plot_graphs():
    analysis_details = analyze()
    corona_by,covid19_by = analysis_details
    fig1,ax1 = plt.subplots(figsize=(10,5))
    ax1.plot(covid19_by['created'],covid19_by['numbers_of_tweet'], label = 'tweets by keyword_name')
    ax1.plot(corona_by['numbers_of_tweet'], label = 'tweets by ketword name')
    ax1.set_ylabel('Number of Tweets')
    ax1.set_xlabel('Created time')
    ax1.set_title ('Amount of tweet by created time')
    
    list_of_figures = [plt.figure(i) for i in plt.get_fignums()]
    return list_of_figures


# In[153]:


plot_graphs()


# In[ ]:




